class TestPage{
    
get acceptCookies(){
    return $('.optanon-alert-box-button-middle.accept-cookie-container');
}
get PopularfiltersBtn(){           
   return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[1]/div/div/button/div');
}
get bestdeals(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[1]/div[2]/div/div[1]/div/div/div[1]/div/div/label');
}
get saveBtn(){
    return $('.sc-bYEvPH.WidthContainedButton-sc-1an5ad7.gtczwt.cVMtBp');
}
get bestdealsResult(){
    return $$('.sc-gsTCUz.TextWithEllipses-sc-1zwi8o.dPzAyJ.kvEBnZ');
    }
get getbestDealsText(){
        return this.bestdealsResult.filter(element => {
            console.log(element.getText());
        })
        
}
get stressPlanbtn(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[1]/div[2]/div/div[1]/div/div/div[3]/div/div');
}
get stressPlanResult(){
    return $$('.sc-gsTCUz.TextWithEllipses-sc-1zwi8o.dPzAyJ.kvEBnZ');
}
get getstressPlanText(){
    return this.stressPlanResult.filter(element=>{
        console.log(element.getText());
    })
}
get confYourselfBtn(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[1]/div[2]/div/div[1]/div/div/div[2]/div/div/label/span');
}
get confYouselfSum2(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div/div[1]/div/div[2]/span/span');
}
get confYouselfSumText(){
    return this.confYouselfSum2.getText();
}
get makeAndModel(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[2]/div/div/button/div');
}
get mersedes(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div[3]/div/div/label/div');
}
get mersedesA(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div[4]/div/div[1]/div/div/label/div');
}
get mersedesASum(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div/div[1]/div/div[2]/span/span');
}
get mersedesASumText(){
    return this.mersedesASum.getText();
}
get landRover(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[2]/div[2]/div/div[2]/div/div/div[2]/div[2]/div/div[15]/div/div/label/span');
}
get evoque(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[2]/div[2]/div/div[2]/div/div/div[2]/div[2]/div/div[16]/div/div[3]/div/div/label/div');
}
get evoqueElem(){
    return $('.sc-ezrdKe.gdMbkz');
}
get evoqueElemText(){
    return this.evoqueElem.getText();
}
get MonthlyPrice(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[3]/div/div/button/div');
}
get slideMaxHandle1(){
    return $('.rc-slider-handle.rc-slider-handle-1');
}
get sliderMaxHandle2(){
    return $('.rc-slider-handle.rc-slider-handle-2');
}
get resultOfMax(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div/div[1]/div/div[2]/span/span')
}
get resultOfMaxText(){
    return this.resultOfMax.getText();
}
get resultOfMin(){
    return $('.sc-gKsewC.Gidpx');
}
get resultOfMinText(){
    return this.resultOfMin.getText();
}
get termMileage(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[4]/div/div/button/div');
}
get forthDot(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[4]/div[2]/div/div[1]/div/div/div[1]/div[3]/div[4]');
}
get firstDot(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[4]/div[2]/div/div[1]/div/div/div[1]/div[3]/div[3]/span[1]');
}
get termResult(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div/div[1]/div/div[2]/span/span');
}
get termResultText(){
    return this.termResult.getText();
}
get secondDot(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[4]/div[2]/div/div[1]/div/div/div[1]/div[3]/div[3]/span[2]');
}
get thirdDot(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[4]/div[2]/div/div[1]/div/div/div[1]/div[3]/div[3]/span[3]');
}
get secondDotResult(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[4]/div/div/div[1]/div/div[2]/span/span');
}
get secondDotResultText(){
    return this.secondDotResult.getText();
}
get fuelType(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[5]/div[1]/div/button/div');
}
get cng(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[5]/div[2]/div/div[1]/div/div/div[1]/div/div/label/div');
}
get cngResult(){
    return $$('.MainContent-sc-1g3c3uc.lolkLE');
}
get cngResultisDisplayed(){
    return this.cngResult.filter(element=>{
        console.log(element.isDisplayed());
    })
}
get moreFilters(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[2]/div[1]/div/div/button/div');
}
get audi(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div[1]/div/div/label/div');
}
get tesla(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div[5]/div/div/label/div');
}
get hybrid(){
    return $('//*[@id="app"]/div/div/div/div/div/div[4]/div[1]/div[3]/div/div/div/div/div/div[1]/div[5]/div[2]/div/div[1]/div/div/div[4]/div/div/label/div');
}
}



module.exports = new TestPage();