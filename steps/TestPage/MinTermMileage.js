const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search MIN value of Term and Mileage", ()=> {
    testpage.termMileage.click();
    testpage.forthDot.dragAndDrop(testpage.firstDot);
    testpage.saveBtn.click();
    browser.pause(2000);
    })

Then("results of related cars with MIN value of Term - 24 months are displayed", ()=>{
    if(testpage.termResultText == '4766 to choose from'){
        console.log(`Result of a MIN value of Term - 24 months serching is correct`)
    } else{
        console.log(`Consider an issue`);
     }  
})
