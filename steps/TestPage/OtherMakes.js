const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search for 'EVOQUE Land Rover'", ()=> {
    testpage.makeAndModel.click();
    testpage.landRover.click();
    testpage.evoque.click();
    testpage.saveBtn.click();
    browser.pause(2000);
    })

Then("results of related cars with a 'EVOQUE Land Rover' mark are displayed", ()=> {
    if(testpage.evoqueElemText == 'Land Rover Evoque'){
        console.log(`Result of a 'Land Rover Evoque' serching is correct`)
    } else{
        console.log(`Consider an issue`);
     }           
})