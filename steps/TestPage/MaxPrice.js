const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search a MAX price of cars", ()=> {
    testpage.MonthlyPrice.click();
    testpage.slideMaxHandle1.dragAndDrop(testpage.sliderMaxHandle2);
    testpage.saveBtn.click();
    browser.pause(2000);
    })

Then("results of related cars with MAX price are displayed", ()=>{
    if(testpage.resultOfMaxText == '2 to choose from'){
        console.log(`Result of a 'Max price' serching is correct`)
    } else{
        console.log(`Consider an issue`);
     }  
})
