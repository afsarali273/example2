const {When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

When("the user search AVG value of Term - 48 months and Mileage", ()=> {
    testpage.termMileage.click();
    testpage.forthDot.dragAndDrop(testpage.thirdDot);
    testpage.saveBtn.click();
    browser.pause(2000);
    })

Then("results of related cars with AVG value of Term - 48 months are displayed", ()=>{
    if(testpage.secondDotResultText == '4782 to choose from'){
        console.log(`Result of AVG value of Term - 48 months serching is correct`)
    } else{
        console.log(`Consider an issue`);
     }  
})
