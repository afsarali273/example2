const {Given, When, Then} = require('cucumber');
const testpage = require('../../pageObject/testpage');

Given("the browser is at Our Vehicles page", () =>{
browser.url('https://www.leaseplan.com/en-be/business/showroom/');
});

When("the user search for 'Best Deals'", ()=> {
browser.maximizeWindow();
testpage.acceptCookies.click();
testpage.PopularfiltersBtn.click();
testpage.bestdeals.click();
testpage.saveBtn.click();
browser.pause(3000);
})

Then("results of related cars with 'Best Deals' mark are displayed", ()=> {
    testpage.getbestDealsText;              
}); 


// Then("the user search for 'Best Deals'", ()=> {
   
// })